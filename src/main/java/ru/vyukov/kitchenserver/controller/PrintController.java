package ru.vyukov.kitchenserver.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.vyukov.kitchenserver.domain.Order;
import ru.vyukov.kitchenserver.service.PrintingService;

@Slf4j
@RestController
public class PrintController {


    @Autowired
    private PrintingService printingService;

    @PostMapping("/table/{tableId}/")
    public void print(@PathVariable("tableId") String tableId, @RequestParam("order") String order) {
        log.info("Заказ: " + tableId + " " + order);
        printingService.print(new Order(tableId,order));
    }
}
