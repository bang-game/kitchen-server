package ru.vyukov.kitchenserver.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.vyukov.kitchenserver.domain.Order;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.io.ByteArrayInputStream;
import java.io.InputStream;


@Slf4j
@Service
@RequiredArgsConstructor
public class PrintingServiceImpl implements PrintingService {


    @Override
    public void print(Order order) {
        try {
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            pras.add(new Copies(1));
            PrintService[] pss = PrintServiceLookup.lookupPrintServices(DocFlavor.INPUT_STREAM.GIF, pras);
            if (pss.length == 0)
                throw new RuntimeException("No printer services available.");
            PrintService ps = pss[0];

            DocPrintJob job = ps.createPrintJob();

            InputStream inputStream = new ByteArrayInputStream(order.toString().getBytes());
            Doc doc = new SimpleDoc(inputStream, DocFlavor.INPUT_STREAM.TEXT_PLAIN_UTF_8, null);
            job.print(doc, pras);
        } catch (PrintException pe) {
            log.error("PrintException ", pe);
        }
    }


}
