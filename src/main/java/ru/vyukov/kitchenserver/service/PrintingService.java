package ru.vyukov.kitchenserver.service;


import ru.vyukov.kitchenserver.domain.Order;

public interface PrintingService {
    void print(Order order);
}
