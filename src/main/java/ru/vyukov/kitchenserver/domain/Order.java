package ru.vyukov.kitchenserver.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private String table;
    private String order;
}
