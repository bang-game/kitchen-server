# Сервер для печати заказов на кухне

REST API сервис
принимает запрос
````http request
POST http://localhost:8080/table/{tableId}/ 
order=текст заказа
````
PathVariable tableId - строка с названием стола

Параметры запроса

order -- текст заказа


пример отправки запроса
```bash
 curl -X POST 'http://localhost:8080/table/webdebug/' --data 'order=текстзаказа'
```